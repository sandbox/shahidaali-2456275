<?php
/**
 * @file
 * Testimonials controller classes
 *
 */
 
/**
 * Controller for loading, creating, and saving testimonials.
 *
 * The default loader, which we extend, handles load() already. We only
 * need to add saving and creating.
 */
class TestimonialsController extends DrupalDefaultEntityController {

  function attachLoad(&$queried_testimonials, $revision_id = FALSE) {
    // Build an array of user picture IDs so that these can be fetched later.
    $picture_fids = array();
    foreach ($queried_testimonials as $key => $record) {
      $picture_fids[] = $record->author_picture;
    }

    // Add the full file objects for user pictures if enabled.
    if (!empty($picture_fids)) {
      $pictures = file_load_multiple($picture_fids);
      foreach ($queried_testimonials as $testimonial) {
        if (!empty($testimonial->author_picture) && isset($pictures[$testimonial->author_picture])) {
          $testimonial->author_picture = $pictures[$testimonial->author_picture];
        }
        else {
          $testimonial->author_picture = NULL;
        }
      }
    }
    // Call the default attachLoad() method. This will add fields and call
    // hook_testimonials_load().
    parent::attachLoad($queried_testimonials, $revision_id);
  }
	
  public function save($testimonial) {
    $transaction = db_transaction();

    try {
      global $user;

      // Determine if we will be inserting a new testimonial.
      $testimonial->is_new = empty($testimonial->tid);

      // Set the timestamp fields.
      if (empty($testimonial->created)) {
        $testimonial->created = REQUEST_TIME;
      }
      $testimonial->changed = REQUEST_TIME;

      $update_testimonial = TRUE;

      // Give modules the opportunity to prepare field data for saving.
      field_attach_presave('testimonials', $testimonial);
			
      // Save picture id instead of object
			if(isset($testimonial->author_picture->fid) && !empty($testimonial->author_picture->fid)){
				$testimonial->author_picture = $testimonial->author_picture->fid;
			}
			
      // If this is a new testimonial...
      if ($testimonial->is_new) {
        // Save the new testimonial.
        drupal_write_record('testimonials', $testimonial);

        $op = 'insert';
      }
      else {
        // Save the updated testimonial.
        drupal_write_record('testimonials', $testimonial, 'tid');

        $op = 'update';
      }

      // Save fields.
      $function = 'field_attach_' . $op;
      $function('testimonials', $testimonial);

      module_invoke_all('entity_' . $op, $testimonial, 'testimonials');

      // Clear internal properties.
      unset($testimonial->is_new);

      // Ignore slave server temporarily to give time for the saved order to be
      // propagated to the slave.
      db_ignore_slave();

      return $testimonial;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('testimonials', $e, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Deletes multiple testimonials by ID.
   *
   * @param $tids
   *   An array of testimonial IDs to delete.
   * @return
   *   TRUE on success, FALSE otherwise.
   */
  public function delete($tids) {
    if (!empty($tids)) {
      $testimonials = $this->load($tids, array());

      $transaction = db_transaction();

      try {
        db_delete('testimonials')
          ->condition('tid', $tids, 'IN')
          ->execute();

        foreach ($testimonials as $testimonial_id => $testimonial) {
          field_attach_delete('testimonials', $testimonial);
        }

        // Ignore slave server temporarily to give time for the
        // saved testimonial to be propagated to the slave.
        db_ignore_slave();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('testimonials', $e, NULL, WATCHDOG_ERROR);
        return FALSE;
      }

      module_invoke_all('entity_delete', $testimonial, 'testimonials');

      // Clear the page and block and testimonial caches.
      cache_clear_all();
      $this->resetCache();
    }

    return TRUE;
  }

  /**
   * Create a default testimonial.
   *
   * @param $type
   *   The machine-readable type of the testimonial.
   *
   * @return
   *   An testimonial object with all default fields initialized.
   */
  public function create($type = '') {
    return (object) array(
      'tid' => 0,
      'uid' => 0,
      'type' => $type,
      'title' => '',
      'message' => '',
      'author_name' => '',
      'author_link' => '',
      'author_picture' => 0,
      'company' => '',
      'rating' => 0,
      'status' => 1,
      'weight' => 0,
    );
  }
	
  /**
   * Build view page
   *
   * @param $entity
   *   The machine-readable type of the testimonial.
   *
   * @return
   *   An testimonial object with all default fields initialized.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('testimonials', $entity);
    $content['author'] = array('#markup' => t('Created by: !author', array('!author' => $wrapper->uid->name->value(array('sanitize' => TRUE)))));

    // Make Description and Status themed like default fields.
    $content['description'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' =>t('Description'),
      '#access' => TRUE,
      '#label_display' => 'above',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#entity_type' => 'testimonials',
      '#bundle' => $entity->type,
      '#items' => array(array('value' => $entity->description)),
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($entity->description))
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}

/**
 * Testimonials class.
 */
class TestimonialsEntity extends Entity {
  public $type;
  public $title;
  public $weight = 0;

  public function __construct($values = array()) {
    parent::__construct($values, 'testimonials');
  }

  function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}


/**
 * UI controller for Testimonials.
 */
class TestimonialsUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage your testimonials.';
    return $items;
  }
}

class TestimonialsTypeController extends EntityAPIControllerExportable {
   public function create(array $values = array()) {
    $values += array(
      'label' => '',
      'description' => '',
    );
    return parent::create($values);
  }

  /**
   * Save Testimonials Type.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);
    // Rebuild menu registry. We do not call menu_rebuild directly, but set
    // variable that indicates rebuild in the end.
    // @see http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }
}

/**
 * UI controller for Testimonials Type.
 */
class TestimonialsTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage Testimonials types.';
    return $items;
  }
}
/**
 * Testimonials Type class.
 */
class TestimonialsType extends Entity {
  public $type;
  public $label;
  public $weight = 0;

  public function __construct($values = array()) {
    parent::__construct($values, 'testimonials_type');
  }

  function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}
