<?php
/**
 * @file 
 * testimonials.pages.inc
 */

function testimonials_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $testimonial = $form['#testimonial'];
  $form_state['redirect'] = array('testimonials/' . $testimonial->tid . '/delete', array('query' => $destination));
}

/**
 * Displays an testimonial.
 *
 * @param $testimonial
 *   The testimonial object to display.
 * @param $view_mode
 *   The view mode we want to display.
 */
function testimonials_page_view($testimonial, $view_mode = 'full') {
  // Remove previously built content, if exists.
  $testimonial->content = array();

	$options['id'] = 'testimonials-single-view';
	$testimonial->content['testimonial'] = array(
    '#markup' => theme('testimonials_list', array('testimonials' => array($testimonial), 'options' => $options))
  );
	
	// Build fields content.
  field_attach_prepare_view('testimonials', array($testimonial->tid => $testimonial), $view_mode);
  entity_prepare_view('testimonials', array($testimonial->tid => $testimonial));
  $testimonial->content += field_attach_view('testimonials', $testimonial, $view_mode);

  return $testimonial->content;
}


/**
 * Menu callback; Displays a listing of recent testimonials.
 *
 * This doesn't really work yet because our presentation code doesn't show
 * the title.
 */
function testimonials_page_list_recent() {
  $content = array();

  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'testimonials')
    ->propertyOrderBy('created', 'DESC')
    ->range(0, 5);
  $result = $query->execute();

  $testimonials = testimonials_load_multiple(array_keys($result['testimonials']));
  foreach ($testimonials as $testimonial) {
    $content[$testimonial->tid] = testimonials_page_view($testimonial, 'teaser');
  }

  return $content;
}
