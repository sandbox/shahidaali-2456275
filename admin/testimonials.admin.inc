<?php
/**
 * @file testimonials.admin.inc
 * Testimonials admin settings and options
 *
 */
 
/**
 * Menu callback; Displays a list of all testimonials.
 */
function testimonials_admin_overview_form() {
  $query = db_select('testimonials', 't');
  $result = $query
    ->fields('t')
		->orderBy('weight', 'ASC')
    ->execute();
		
	$form['#tree'] = TRUE;
	$form['testimonials'] = array();
  foreach ($result as $testimonial) {
		// This field is invisible, but contains sort info (weights).
		$form['testimonials'][$testimonial->tid]['weight'] = array(
			'#type' => 'weight',
			'#title' => t('Weight'),
			'#title_display' => 'invisible',
			'#default_value' => $testimonial->weight,
		);
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save changes'));
	return $form;
}
/**
 * Menu callback; Displays a list of all testimonials.
 */
function theme_testimonials_admin_overview_form($variables)  {
	$form = $variables['form'];
  $destination = drupal_get_destination();
	
	$form['#tree'] = TRUE;
  $rows = array();
	
	$header = array();
	$header[] = array('data' => t('Title'));
	$header[] = array('data' => t('Type'));
	$header[] = array('data' => t('Message'));
	$header[] = array('data' => t('Author'));
	$header[] = array('data' => t('Picture'));
	$header[] = array('data' => t('Rating'));
	
	$columns = array(
		//'status' => t('Status'),
	);
	foreach ($columns as $key => $title) {
		$header[] = array('data' => $title);
	}
	
	$header[] = array('data' => t('Weight'));
	$header[] = array('data' => t('Operations'));
	
  foreach (element_children($form['testimonials']) as $testimonial_id) {
		$testimonial = testimonials_load($testimonial_id);
		$form['testimonials'][$testimonial_id]['weight']['#attributes']['class'] = array('testimonials-order-weight');
		$type = testimonials_type_load($testimonial->type);
			
		
    $row = array();
    $row[] = l($testimonial->title, 'testimonials/' . $testimonial->tid, array('query' => $destination));
    $row[] = array('data' => $type->label);
    $row[] = array('data' => drupal_substr($testimonial->message, 0, 50) . '...');
    $row[] = array('data' => theme('testimonials_author_name', array('testimonial' => $testimonial)));
    $row[] = array('data' => theme('testimonials_picture', array('testimonial' => $testimonial)));
    $row[] = array('data' => theme('testimonials_rating', array('testimonial' => $testimonial)));
		
    foreach ($columns as $key => $title) {
      $row[] = array('data' => $testimonial->$key);
    }
		
		$row[] = drupal_render($form['testimonials'][$testimonial_id]['weight']);
    $operations = l(t('edit'), 'testimonials/' . $testimonial->tid . '/edit', array('query' => $destination));
		$operations .= '&nbsp;&nbsp;&nbsp;' . l(t('delete'), 'testimonials/' . $testimonial->tid . '/delete', array('query' => $destination));
    $row[] = $operations;

		$rows[] = array(
      'data' => $row,
      'class' => array('draggable'),
    );
  }
  if (empty($rows)) {
    $rows[] = array(
      array('data' => t('No testimonials added yet.'), 'colspan' => count($header)),
    );
  }
	
	$output = '';
	
	$output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'testimonials-order')));
  $output .= drupal_render_children($form);
	
	drupal_add_tabledrag('testimonials-order', 'order', 'sibling', 'testimonials-order-weight');
	
  return $output;
}



function testimonials_admin_overview_form_submit($form, &$form_state) {
	if(!empty($form_state['values']['testimonials'])){
		$testimonials = array();
		foreach ($form_state['values']['testimonials'] as $tid => $testimonial) {
			$testimonials[] = array(
				'tid' => $tid,
				'weight' => $testimonial['weight'],
			);
		}
		
		if (!empty($testimonials)) {
			usort($testimonials, '_testimonials_arraysort');
		}
	
		foreach ($testimonials as $testimonials_weight) {
			$testimonial = testimonials_load($testimonials_weight['tid']);
			$testimonial->weight = $testimonials_weight['weight'];
			testimonials_save($testimonial);
		}
	}
  drupal_set_message(t('Changes saved.'));
}

// Custom array sort function by weight.
function _testimonials_arraysort($a, $b) {
  if (isset($a['weight']) && isset($b['weight'])) {
    return $a['weight'] < $b['weight'] ? -1 : 1;
  }
  return 0;
}

/**
 * Form builder; Configure testimonial settings for this site.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function testimonials_admin_settings() {
  // Block settings.
  $form['block_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Block settings'),
  );
	$form['block_settings']['testimonials_blocks_count'] = array(
		'#type' => 'textfield',
		'#title' => t('Number of testimonials blocks'),
		'#default_value' => variable_get('testimonials_blocks_count', 1),
		'#description' => t('Number of testimonials blocks availabe in blocks.'),
	);
  // Display settings.
  $form['display_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display settings'),
  );
  // If picture support is enabled, check whether the picture directory exists.
	$picture_path =  file_default_scheme() . '://' . variable_get('testimonials_picture_path', 'testimonials');
	if (!file_prepare_directory($picture_path, FILE_CREATE_DIRECTORY)) {
		form_set_error('testimonials_picture_path', t('The directory %directory does not exist or is not writable.', array('%directory' => $picture_path)));
		watchdog('file system', 'The directory %directory does not exist or is not writable.', array('%directory' => $picture_path), WATCHDOG_ERROR);
	}
	
	// Pictures
  $form['display_settings']['pictures'] = array(
    '#type' => 'fieldset',
    '#title' => t('Picture settings'),
  );
  $form['display_settings']['pictures']['testimonials_picture_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Picture directory'),
    '#default_value' => variable_get('testimonials_picture_path', 'testimonials'),
    '#size' => 30,
    '#maxlength' => 255,
    '#description' => t('Subdirectory in the file upload directory where pictures will be stored.'),
  );
  $form['display_settings']['pictures']['testimonials_picture_default'] = array(
    '#type' => 'textfield',
    '#title' => t('Default picture'),
    '#default_value' => variable_get('testimonials_picture_default', ''),
    '#size' => 30,
    '#maxlength' => 255,
    '#description' => t('URL of picture to display for testimonials with no custom picture selected. Leave blank for none.'),
  );
  if (module_exists('image')) {
    $form['display_settings']['pictures']['settings']['testimonials_picture_style'] = array(
      '#type' => 'select',
      '#title' => t('Picture display style'),
      '#options' => image_style_options(TRUE, PASS_THROUGH),
      '#default_value' => variable_get('testimonials_picture_style', ''),
      '#description' => t('The style selected will be used on display, while the original image is retained. Styles may be configured in the <a href="!url">Image styles</a> administration area.', array('!url' => url('admin/config/media/image-styles'))),
    );
  }
  $form['display_settings']['pictures']['testimonials_picture_dimensions'] = array(
    '#type' => 'textfield',
    '#title' => t('Picture upload dimensions'),
    '#default_value' => variable_get('testimonials_picture_dimensions', '85x85'),
    '#size' => 10,
    '#maxlength' => 10,
    '#field_suffix' => ' ' . t('pixels'),
    '#description' => t('Pictures larger than this will be scaled down to this size.'),
  );
  $form['display_settings']['pictures']['testimonials_picture_shape'] = array(
		'#type' => 'select',
		'#title' => t('Picture shape'),
		'#options' => testimonials_options('picture_shape'),
		'#default_value' => variable_get('testimonials_picture_shape', 'rounded'),
    '#description' => t('Pictures default display.'),
  );
	
	// Theme
  $form['display_settings']['theme'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme settings'),
  );
	$form['display_settings']['theme']['testimonials_default_theme'] = array(
		'#type' => 'select',
		'#title' => t('Testimonials default theme'),
		'#options' => testimonials_options('themes'),
		'#default_value' => variable_get('testimonials_default_theme', 'speech'),
		'#description' => t('The theme selected will be used on all displays by default.'),
	);
	$form['display_settings']['theme']['testimonials_default_composition'] = array(
		'#type' => 'select',
		'#title' => t('Testimonials default composition'),
		'#options' => testimonials_options('composition'),
		'#default_value' => variable_get('testimonials_default_composition', 'info_above'),
		'#description' => t('The composition selected will be used on all displays by default.<br /> Note: for "Author Below, Text Above" or "Author Above, Text Below" please rearrange views fields for views display.'),
	);
	$form['display_settings']['theme']['testimonials_default_alignment'] = array(
		'#type' => 'select',
		'#title' => t('Testimonials default alignment'),
		'#options' => testimonials_options('alignment'),
		'#default_value' => variable_get('testimonials_default_alignment', 'center'),
		'#description' => t('The alignment selected will be used on all displays by default.'),
	);
  return system_settings_form($form);
}

/**
 * Menu callback; Show list of testimonials types we can add.
 */
function testimonials_add_page() {
  $types = testimonials_types();
	$items = array();
  foreach ($types as $type => $info) {
    $items[] = l($info->label, 'admin/structure/testimonials/add/' . $type);
  }
	
	$output = '';
	if(empty($items)){
		$output .= t('Please add a testimonial type to add testimonials. !url to add new testimonial type.', array('!url' => l('Click here', 'admin/structure/testimonials/testimonials-types/add')));
	}
	else {
		$output .= '<p>' . t('Please choose testimonials type to add testimonials.') . '</p>';
		$output .= theme('item_list', array('items' => $items));
	}
  return $output;
}

/**
 * Present an testimonials submission form.
 */
function testimonials_add($type) {
  global $user;

  $types = testimonials_types();
  $type = isset($type) ? str_replace('-', '_', $type) : NULL;
  if (empty($types[$type])) {
    return MENU_NOT_FOUND;
  }

  $testimonial = entity_get_controller('testimonials')->create($type);

  drupal_set_title(t('Create @name', array('@name' => $types[$type]->label)), PASS_THROUGH);
  return drupal_get_form($type . '_testimonials_form', $testimonial);
}

/**
 * Menu callback; presents the testimonial editing form, or redirects to delete confirmation.
 *
 * @param $testimonial
 *   The testimonial object to edit.
 */
function testimonials_page_edit($testimonial) {
  $types = testimonials_types();
  drupal_set_title(t('<em>Edit @type</em> @title', array('@type' => $types[$testimonial->type]->label, '@title' => $testimonial->title)), PASS_THROUGH);

  return drupal_get_form($testimonial->type . '_testimonials_form', $testimonial);
}
/**
 * Form builder; Displays the testimonials add/edit form.
 *
 * @param $form
 * @param $form_state
 * @param $testimonial
 *   The testimonial object to edit, which may be brand new.
 */
function testimonials_form($form, &$form_state, $testimonial) {

  // Set the id and identify this as an testimonials edit form.
  $form['#id'] = 'testimonials-form';

  // Save the testimonial for later, in case we need it.
  $form['#testimonial'] = $testimonial;
  $form_state['testimonial'] = $testimonial;

  // Common fields. We don't have many.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $testimonial->title,
    '#weight' => 1,
    '#required' => TRUE,
  );

  // Common fields. We don't have many.
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Testimonial Message'),
    '#default_value' => $testimonial->message,
    '#weight' => 1,
    '#required' => TRUE,
  );

  // Common fields. We don't have many.
  $form['author_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Author name'),
    '#default_value' => $testimonial->author_name,
    '#weight' => 2,
    '#required' => TRUE,
  );

  // Common fields. We don't have many.
  $form['author_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Author link'),
    '#default_value' => $testimonial->author_link,
    '#weight' => 3,
    '#required' => FALSE,
  );
	
  // Common fields. We don't have many.
  $form['company'] = array(
    '#type' => 'textfield',
    '#title' => t('Company name'),
    '#default_value' => $testimonial->company,
    '#weight' => 4,
    '#required' => FALSE,
  );

  // Common fields. We don't have many.
  $form['rating'] = array(
    '#type' => 'select',
    '#title' => t('Rating'),
		'#options' => drupal_map_assoc(array(1, 2, 3, 4, 5)),
		'#empty_value' => t('- Select -'),
    '#default_value' => $testimonial->rating,
    '#weight' => 5,
    '#required' => FALSE,
  );
	
  // Picture/avatar.
  $form['author_picture'] = array(
    '#type' => 'fieldset',
    '#title' => t('Picture'),
		'#weight' => 6,
  );
  $form['author_picture']['author_picture'] = array(
    '#type' => 'value',
    '#value' => isset($testimonial->author_picture) ? $testimonial->author_picture : NULL,
  );
  $form['author_picture']['picture_current'] = array(
    '#markup' => theme('testimonials_picture', array('testimonial' => $testimonial)),
  );
  $form['author_picture']['picture_delete'] = array(
    '#type' => 'checkbox',
    '#title' => t('Delete picture'),
    '#access' => !empty($testimonial->author_picture->fid),
    '#description' => t('Check this box to delete your current picture.'),
  );
  $form['author_picture']['picture_upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload picture'),
    '#size' => 48,
  );
  $form['#validate'][] = 'testimonials_validate_picture';
	
  // Add the buttons.
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 7,
    '#submit' => array('testimonials_form_submit'),
  );
  if (!empty($testimonial->tid)) {
    $form['buttons']['delete'] = array(
      '#access' => user_access('delete testimonials'),
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('testimonials_form_delete_submit'),
    );
  }

  $form['#validate'][] = 'testimonials_form_validate';

  field_attach_form('testimonials', $testimonial, $form, $form_state);

  return $form;
}

/**
 * Validates an image uploaded by a user.
 *
 * @see testimonials_form()
 */
function testimonials_validate_picture(&$form, &$form_state) {
  // If required, validate the uploaded picture.
  $validators = array(
    'file_validate_is_image' => array(),
    'file_validate_image_resolution' => array(variable_get('testimonials_picture_dimensions', '85x85')),
    'file_validate_size' => array(2 * 1024 * 1024), // max 2mb
  );

  // Save the file as a temporary file.
  $file = file_save_upload('picture_upload', $validators);
  if ($file === FALSE) {
    form_set_error('picture_upload', t("Failed to upload the picture image; the %directory directory doesn't exist or is not writable.", array('%directory' => 'testimonials')));
  }
  elseif ($file !== NULL) {
    $form_state['values']['picture_upload'] = $file;
  }
}

function testimonials_form_validate($form, &$form_state) {
  $testimonial = $form_state['testimonial'];

  // Field validation.
  field_attach_form_validate('testimonials', $testimonial, $form, $form_state);
}


function testimonials_form_submit($form, &$form_state) {
  global $user;

  $testimonial = &$form_state['testimonial'];

	// Load the stored entity, if any.
	if (!empty($testimonial->tid) && !isset($testimonial->original)) {
		$testimonial->original = entity_load_unchanged('testimonials', $testimonial->tid);
	}
	
  // Set the testimonial's uid if it's being created at this time.
  if (empty($testimonial->uid)) {
    $testimonial->uid = $user->uid;
  }
	
	// Process picture uploads.
	if (isset($form_state['values']['picture_upload']->fid) && !empty($form_state['values']['picture_upload']->fid)) {
		$author_picture = $form_state['values']['picture_upload'];
		// If the author_picture is a temporary file move it to its final location and
		// make it permanent.
		if (!$author_picture->status) {
			$info = image_get_info($author_picture->uri);
			$author_picture_directory =  file_default_scheme() . '://' . variable_get('testimonials_picture_path', 'testimonials');

			// Prepare the author_pictures directory.
			file_prepare_directory($author_picture_directory, FILE_CREATE_DIRECTORY);
			$destination = file_stream_wrapper_uri_normalize($author_picture_directory . '/testimonials-picture-' . $testimonial->tid . '-' . REQUEST_TIME . '.' . $info['extension']);

			// Move the temporary file into the final location.
			if ($author_picture = file_move($author_picture, $destination, FILE_EXISTS_RENAME)) {
				$author_picture->status = FILE_STATUS_PERMANENT;
				$testimonial->author_picture = file_save($author_picture);
				file_usage_add($author_picture, 'testimonials', $testimonial->type, $testimonial->tid);
			}
		}
		// Delete the previous author_picture if it was deleted or replaced.
		if (!empty($testimonial->original->author_picture->fid)) {
			file_usage_delete($testimonial->original->author_picture, 'testimonials', $testimonial->type, $testimonial->tid);
			file_delete($testimonial->original->author_picture);
		}
	}
	elseif (isset($form_state['values']['picture_delete']) && $form_state['values']['picture_delete']) {
		file_usage_delete($testimonial->original->author_picture, 'testimonials', $testimonial->type, $testimonial->tid);
		file_delete($testimonial->original->author_picture);
	}
	// Save the author_picture object, if it is set. drupal_write_record() expects
	// $testimonial->author_picture to be a FID.
	$author_picture = empty($testimonial->author_picture) ? NULL : $testimonial->author_picture;
	$testimonial->author_picture = empty($testimonial->author_picture->fid) ? 0 : $testimonial->author_picture->fid;

  $testimonial->title = $form_state['values']['title'];
  $testimonial->message = $form_state['values']['message'];
  $testimonial->author_name = $form_state['values']['author_name'];
  $testimonial->company = $form_state['values']['company'];
  $testimonial->author_link = $form_state['values']['author_link'];
  $testimonial->rating = $form_state['values']['rating'];

  // Notify field widgets.
  field_attach_submit('testimonials', $testimonial, $form, $form_state);

  // Save the testimonial.
  testimonials_save($testimonial);

  // Notify the user.
  drupal_set_message(t('Testimonial saved.'));

  $form_state['redirect'] = 'testimonials/' . $testimonial->tid;
}


/**
 * Form bulder; Asks for confirmation of testimonial deletion.
 */
function testimonials_delete_confirm($form, &$form_state, $testimonial) {
  $form['#testimonial'] = $testimonial;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['tid'] = array('#type' => 'value', '#value' => $testimonial->tid);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $testimonial->title)),
    'testimonials/' . $testimonial->tid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Executes testimonial deletion.
 */
function testimonials_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $testimonial = testimonials_load($form_state['values']['tid']);
    testimonials_delete($form_state['values']['tid']);
    watchdog('testimonials', '@type: deleted %title.', array('@type' => $testimonial->type, '%title' => $testimonial->title));

    $types = testimonials_types();
    drupal_set_message(t('@type %title has been deleted.', array('@type' => $types[$testimonial->type]->label, '%title' => $testimonial->title)));
  }

  $form_state['redirect'] = '<front>';
}
/**
 * Generates the testimonials type editing form.
 */
function testimonials_type_form($form, &$form_state, $testimonials_type, $op = 'edit') {

  if ($op == 'clone') {
    $testimonials_type->label .= ' (cloned)';
    $testimonials_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $testimonials_type->label,
    '#description' => t('The human-readable name of this testimonials type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($testimonials_type->type) ? $testimonials_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $testimonials_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'testimonials_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this testimonials type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($testimonials_type->description) ? $testimonials_type->description : '',
    '#description' => t('Description about the testimonials type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save testimonials type'),
    '#weight' => 40,
  );

  if (!$testimonials_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete testimonials type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('testimonials_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing testimonials_type.
 */
function testimonials_type_form_submit(&$form, &$form_state) {
  $testimonials_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  testimonials_type_save($testimonials_type);

  // Redirect user back to list of testimonials types.
  $form_state['redirect'] = 'admin/structure/testimonials/testimonials-types';
}

function testimonials_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/testimonials/testimonials-types/' . $form_state['testimonials_type']->type . '/delete';
}

/**
 * testimonials type delete form.
 */
function testimonials_type_form_delete_confirm($form, &$form_state, $testimonials_type) {
  $form_state['testimonials_type'] = $testimonials_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['testimonials_type_id'] = array('#type' => 'value', '#value' => entity_id('testimonials_type' ,$testimonials_type));
  return confirm_form($form,
    t('Are you sure you want to delete testimonials type %title?', array('%title' => entity_label('testimonials_type', $testimonials_type))),
    'testimonials/' . entity_id('testimonials_type' ,$testimonials_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * testimonials type delete form submit handler.
 */
function testimonials_type_form_delete_confirm_submit($form, &$form_state) {
  $testimonials_type = $form_state['testimonials_type'];
  testimonials_type_delete($testimonials_type);

  watchdog('testimonials_type', '@type: deleted %title.', array('@type' => $testimonials_type->type, '%title' => $testimonials_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $testimonials_type->type, '%title' => $testimonials_type->label)));

  $form_state['redirect'] = 'admin/structure/testimonials/testimonials-types';
}

/**
 * Menu callback; List all testimonials types available.
 */
function testimonials_overview_types() {
	$items = array();
  foreach (testimonials_types() as $type => $info) {
    $label = t('View @type', array('@type' => $info->label));
    $items[] = l($label, 'admin/structure/testimonials/manage/' . $type);
  }

  return theme('item_list', array('items' => $items));
}

/**
 * Menu callback; Testimonials information page.
 *
 * @param object $testimonials_type
 */
function testimonials_information($testimonials_type) {
  return $testimonials_type->label . ': ' . $testimonials_type->description;
}
