<?php

/**
 * @file
 * Definition of views_handler_field_user_picture.
 */

/**
 * Field handler to provide simple renderer that allows using a themed user link.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_testimonials_picture extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['tid'] = 'tid';
    $this->additional_fields['title'] = 'title';
    $this->additional_fields['author_link'] = 'author_link';
    $this->additional_fields['author_name'] = 'author_name';
  }

  function element_type($none_supported = FALSE, $default_empty = FALSE, $inline = FALSE) {
    if ($inline) {
      return 'span';
    }
    if ($none_supported) {
      if ($this->options['element_type'] === '0') {
        return '';
      }
    }
    if ($this->options['element_type']) {
      return check_plain($this->options['element_type']);
    }
    if ($default_empty) {
      return '';
    }
    if (isset($this->definition['element type'])) {
      return $this->definition['element type'];
    }

    return 'div';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_photo_to_author_link'] = array('default' => FALSE, 'bool' => TRUE);
    $options['image_style'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_photo_to_author_link'] = array(
      '#title' => t("Link to author's link"),
      '#description' => t("Link to author's link if availabel"),
      '#type' => 'checkbox',
      '#default_value' => $this->options['link_photo_to_author_link'],
    );

    if (module_exists('image')) {
      $styles = image_styles();
      $style_options = array('' => t('Default'));
      foreach ($styles as $style) {
        $style_options[$style['name']] = $style['name'];
      }

      $form['image_style'] = array(
        '#title' => t('Image style'),
        '#description' => t('Using <em>Default</em> will use the site-wide image style for testimonial pictures set in the <a href="!testimonials-settings">Testimonials settings</a>.', array('!testimonials-settings' => url('admin/structure/testimonials/settings'))),
        '#type' => 'select',
        '#options' => $style_options,
        '#default_value' => $this->options['image_style'],
      );
    }
  }

  function render($values) {
    if ($this->options['image_style'] && module_exists('image')) {
      // @todo: Switch to always using theme('user_picture') when it starts
      // supporting image styles. See http://drupal.org/node/1021564
      if ($picture_fid = $this->get_value($values)) {
        $picture = file_load($picture_fid);
        $picture_filepath = $picture->uri;
      }
      else {
        $picture_filepath = variable_get('testimonials_picture_default', '');
      }
      if (file_valid_uri($picture_filepath)) {
        $output = theme('image_style', array('style_name' => $this->options['image_style'], 'path' => $picture_filepath));
        if ($this->options['link_photo_to_author_link'] && $this->get_value($values, 'author_link')) {
          $author_link = $this->get_value($values, 'author_link');
          $output = l($output, $author_link, array('html' => TRUE));
        }
      }
      else {
        $output = '';
      }
    }
    else {
      // Fake an testimonial object.
      $testimonial = new stdClass();
      if ($this->options['link_photo_to_author_link']) {
        // Prevent template_preprocess_testimonial_picture from adding a link
        // by not setting the uid.
				$testimonial->author_link = $this->get_value($values, 'author_link');
      }
			else {
				$testimonial->author_link = '';
			}
      $testimonial->title = $this->get_value($values, 'title');
      $testimonial->author_name = $this->get_value($values, 'author_name');
      $testimonial->author_picture = $this->get_value($values);
      $output = theme('testimonials_picture', array('testimonial' => $testimonial));
    }

    return $output;
  }
}
