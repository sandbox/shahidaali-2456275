<?php

/**
 * @file
 * Definition of views_handler_field_testimonials_rendered_entity.
 */

/**
 * Field handler to provide rendered testimonial
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_testimonials_rendered_entity extends views_handler_field {
  function construct() {
    parent::construct();
  }

  function element_type($none_supported = FALSE, $default_empty = FALSE, $inline = FALSE) {
    if ($inline) {
      return 'div';
    }
    if ($none_supported) {
      if ($this->options['element_type'] === '0') {
        return '';
      }
    }
    if ($this->options['element_type']) {
      return check_plain($this->options['element_type']);
    }
    if ($default_empty) {
      return '';
    }
    if (isset($this->definition['element type'])) {
      return $this->definition['element type'];
    }

    return 'div';
  }

  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  function render($values) {
		// Load testimonial
		$testimonial = testimonials_load($this->get_value($values));
		
		$output = theme('testimonials_list_item', array('testimonial' => $testimonial, 'options' => $this->view->style_plugin->options));
    return $output;
  }
}
