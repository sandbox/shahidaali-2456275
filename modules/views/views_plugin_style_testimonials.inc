<?php

/**
 * @file
 * owlcarousel_views_plugin_style_owlcarousel.inc
 */

/**
 * Implements a style type plugin for the Views module.
 */
class testimonials_views_plugin_style_testimonials extends owlcarousel_views_plugin_style_owlcarousel {

  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['instance'] = array('default' => 'default');
		
		// default options
    $options = array_merge($options, testimonials_options('default_settings'));
    return $options;
  }

  /**
   * Show a form to edit the style options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
		
    // Carousel settings.
		// attach testimonials settings form
		$form = $form + testimonials_settings_form($form, $form_state, $this->options);
		
		// remove settings which are not required in views
		if(isset($form['testimonials_settings']['owlcarousel_settings'])){
			unset($form['testimonials_settings']['owlcarousel_settings']);	
		}
		if(isset($form['testimonials_settings']['types'])){
			unset($form['testimonials_settings']['types']);	
		}
		if(isset($form['testimonials_settings']['count'])){
			unset($form['testimonials_settings']['count']);	
		}
  }

  /**
   * Additionally format saved instance.
   */
  function options_submit(&$form, &$form_state) {
    $settings = $form_state['values']['style_options'];
		$owl_settings = $settings['settings'];
		$testimonials_settings = $settings['testimonials_settings'];
    $config = array_merge($owl_settings + $testimonials_settings);

    // Keep default value structure.
    $form_state['values']['style_options'] = $config;
  }
}
