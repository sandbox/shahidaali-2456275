<?php
/**
 * @file
 * testimonials.views.inc
 */
 
/**
 * Implements hook_views_plugins().
 */
function testimonials_views_views_plugins() {
	// add testimonails plugin style
	// this plugin style depends on
	// owlcarousel_views_plugin_style_owlcarousel
	// defined in owlcarousel_views
	$plugins = array(
		'module' => 'testimonials',
		'style' => array(
			'testimonials' => array(
				'title' => t('Testimonials Carousel'),
				'help' => t('Displays the view output in an Testimonials Carousel'),
				'help topic' => '',
				'handler' => 'testimonials_views_plugin_style_testimonials',
				'theme' => 'owlcarousel_views',
				'theme file' => 'owlcarousel_views.theme.inc',
				'theme path' => drupal_get_path('module', 'owlcarousel_views') . '/theme',
				'uses row plugin' => TRUE,
				'uses fields' => TRUE,
				'uses options' => TRUE,
				'type' => 'normal',
				'even empty' => FALSE,
			),
		),
	);
	return $plugins;
}

/**
 * Implements hook_views_data().
 */
function testimonials_views_views_data() {
  $data = array();

  $data['testimonials']['table']['group'] = t('Testimonials');

  $data['testimonials']['table']['base'] = array(
    'field' => 'tid',
    'title' => t('Testimonials'),
    'help' => '',
  );
	
  $data['testimonials']['title'] = array(
    'title' => t('Title'),
    'help' => t('The title of the testimonials.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
	
  $data['testimonials']['message'] = array(
    'title' => t('Message'),
    'help' => t('The message of the testimonials.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
  );
	
  $data['testimonials']['author_name'] = array(
    'title' => t('Author name'),
    'help' => t('The author name of the testimonials.'),
    'field' => array(
      'handler' => 'views_handler_field_testimonials_author_name',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
	
  $data['testimonials']['author_link'] = array(
    'title' => t('Author link'),
    'help' => t('The author link of the testimonials.'),
    'field' => array(
      'handler' => 'views_handler_field_url',
      'click sortable' => FALSE,
    ),
  );
	
  $data['testimonials']['company'] = array(
    'title' => t('Company'),
    'help' => t('The company name of the testimonials.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
	
  $data['testimonials']['tid'] = array(
    'title' => t('Testimonials ID'),
    'help' => t('The testimonial ID of the Testimonials.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
	
  $data['testimonials']['type'] = array(
    'title' => t('Type'),
    'help' => t('The type of the testimonials.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
	
  // picture
  $data['users']['author_picture_fid']['moved to'] = array('testimonials', 'author_picture');
  $data['testimonials']['author_picture'] = array(
    'title' => t('Author Picture'),
    'help' => t("The testimonial author picture."), // The help that appears on the UI,
    // Information for displaying the uid
    'field' => array(
      'handler' => 'views_handler_field_testimonials_picture',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Has Image'),
      'type' => 'yes-no',
    ),
  );
	
  $data['testimonials']['rating'] = array(
    'title' => t('Rating'),
    'help' => t('The rating of the testimonials.'),
    'field' => array(
      'handler' => 'views_handler_field_testimonials_rating',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
	
  $data['testimonials']['status'] = array(
    'title' => t('Status'),
    'help' => t('Whether or not the testimonial is enabled.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Status'),
      'type' => 'yes-no',
      // Use boolean_field = 1 instead of boolean_field <> 0 in WHERE statement.
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
	
	$data['testimonials']['created'] = array(
    'title' => t('Created'),
    'help' => t('When the testimonial was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
	
	$data['testimonials']['changed'] = array(
    'title' => t('Updated'),
    'help' => t('When the testimonial was updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
	
	$data['testimonials']['rendered_entity'] = array(
		'title' => t('Rendered Testimonial'),
		'real field' => 'tid',
		'help' => t('Rendered testimonial, no need to add other fields when added this.'),
    'field' => array(
      'handler' => 'views_handler_field_testimonials_rendered_entity',
    ),
  );
  return $data;
}

