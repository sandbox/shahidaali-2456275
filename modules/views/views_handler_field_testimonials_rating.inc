<?php

/**
 * @file
 * Definition of views_handler_field_testimonials_rating.
 */

/**
 * Field handler to provide simple renderer that allows using a themed testimonial rating.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_testimonials_rating extends views_handler_field {
  function construct() {
    parent::construct();
		//$this->additional_fields['rating'] = 'rating';
  }

  function element_type($none_supported = FALSE, $default_empty = FALSE, $inline = FALSE) {
    if ($inline) {
      return 'span';
    }
    if ($none_supported) {
      if ($this->options['element_type'] === '0') {
        return '';
      }
    }
    if ($this->options['element_type']) {
      return check_plain($this->options['element_type']);
    }
    if ($default_empty) {
      return '';
    }
    if (isset($this->definition['element type'])) {
      return $this->definition['element type'];
    }

    return 'div';
  }

  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  function render($values) {
		
		// Fake an testimonial object.
		$testimonial = new stdClass();
		$testimonial->rating = $this->get_value($values);
    $output = theme('testimonials_rating', array('testimonial' => $testimonial));
    return $output;
  }
}
