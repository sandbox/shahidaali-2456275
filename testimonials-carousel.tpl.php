<?php

/**
 * @file
 * Default theme implementation to present testimonials items (values from testimonials
 * account testimonials fields or modules).
 *
 * This template is used to loop through and render each field configured
 * for the testimonials's account. It can also be the data from modules. The output is
 * grouped by categories.
 *
 * Available variables:
 * - $title: Field title for the testimonials item.
 * - $value: testimonials defined value for the testimonials item or data from a module.
 * - $attributes: HTML attributes. Usually renders classes.
 *
 * @see template_preprocess_testimonials_item()
 */
?>
<div <?php print $wrapper_attributes; ?>>
	<div class="testimonials-wrapper testimonials-clearfix">
		<?php print $carousel; ?>
  </div>
</div>