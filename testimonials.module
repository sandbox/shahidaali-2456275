<?php
/**
 * @file 
 * testimonials.module
 */

/**
 * Implements hook_entity_info().
 */
function testimonials_entity_info() {
  $return['testimonials'] = array(
    'label' => t('Testimonials'),
    'entity class' => 'TestimonialsEntity',
    'controller class' => 'TestimonialsController',
    'admin ui' => array(
      'path' => 'admin/structure/testimonials',
      'file' => 'admin/testimonials.admin.inc',
      'controller class' => 'TestimonialsUIController',
    ),
    'base table' => 'testimonials',
    'uri callback' => 'testimonials_uri',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'tid',
      'bundle' => 'type',
      'label' => 'title',
     ),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'static cache' => TRUE,
		'bundles' => array(),
		'load hook' => 'testimonials_load',
    'view modes' => array(
      'full' => array(
        'label' => t('Full content'),
        'custom settings' => FALSE,
      ),
      'teaser' => array(
        'label' => t('Teaser'),
        'custom settings' => FALSE,
      ),
    ),
		'module' => 'testimonials',
		'access callback' => 'testimonials_access',
  );

  $return['testimonials_type'] = array(
    'label' => t('Testimonials Type'),
    'entity class' => 'TestimonialsType',
    'controller class' => 'TestimonialsTypeController',
    'base table' => 'testimonials_type',
    'fieldable' => FALSE,
    'bundle of' => 'testimonials',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'ttid',
      'name' => 'type',
      'label' => 'label',
    ),
    'module' => 'testimonials',
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/structure/testimonials/testimonials-types',
      'file' => 'admin/testimonials.admin.inc',
      'controller class' => 'TestimonialsTypeUIController',
    ),
    'access callback' => 'testimonials_type_access',
  );
	
  return $return;
}

/**
 * Entity uri callback.
 */
function testimonials_uri($testimonial) {
  return array(
    'path' => 'testimonials/' . $testimonial->tid,
  );
}

/**
 * Implements hook_entity_info_alter().
 */
function testimonials_entity_info_alter(&$entity_info) {
  foreach (testimonials_types() as $type => $info) {
    $entity_info['testimonials']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/structure/testimonials/testimonials-types/manage/%testimonials_type',
        'real path' => 'admin/structure/testimonials/testimonials-types/manage/' . $type,
        'bundle argument' => 5,
      ),
    );
  }
}

/**
 * Implements hook_fied_extra_fields().
 */
function testimonials_field_extra_fields() {
  $extra = array();

  foreach (testimonials_types() as $type) {
    $extra['testimonials'][$type->type] = array(
      'form' => array(
        'title' => array(
          'label' => t('Title'),
          'description' => t('The name of the testimonial'),
          'weight' => -5,
        ),
      ),
      'display' => array(
        'title' => array(
          'label' => t('Title'),
          'description' => t('The name of the testimonial'),
          'weight' => -5,
        ),
      ),
    );
  }

  return $extra;
}

/**
 * Implements hook_element_info().
 */
function testimonials_element_info() {
  $types['testimonials_item'] = array(
    '#theme' => 'testimonials_item',
  );
  return $types;
}
/**
 * Implements hook_theme().
 */
function testimonials_theme() {
  return array(
    'testimonials_carousel' => array(
      'variables' => array('testimonials' => NULL, 'options' => array()),
      'template' => 'testimonials-carousel',
    ),
    'testimonials_list' => array(
      'variables' => array('testimonials' => NULL, 'options' => array()),
      'template' => 'testimonials-list',
    ),
    'testimonials_list_item' => array(
      'variables' => array('testimonial' => NULL, 'options' => array()),
      'template' => 'testimonials-list-item',
    ),
    'testimonials_picture' => array(
      'variables' => array('testimonial' => NULL),
      'template' => 'testimonials-picture',
    ),
    'testimonials_item' => array(
      'render element' => 'element',
      'template' => 'testimonials-item',
    ),
		'testimonials_admin_overview_form' => array(
      'render element' => 'form',
    ),
		'testimonials_author_name' => array(
      'variables' => array('testimonial' => NULL),
    ),
		'testimonials_rating' => array(
      'variables' => array('testimonial' => NULL, 'rating_count' => 5),
    ),
  );
}

/**
 * Implements hook_permission().
 */
function testimonials_permission() {
  return array(
    'administer testimonials types' => array(
      'title' => t('Administer testimonials types'),
      'description' => t('Allows users to configure testimonials types and their fields.'),
    ),
    'administer testimonials' =>  array(
      'title' => t('Administer testimonials'),
			'description' => t('Allows users to configure testimonials.'),
    ),
    'create testimonials' =>  array(
      'title' => t('Create testimonials'),
    ),
    'update testimonials' =>  array(
      'title' => t('Update testimonials'),
    ),
    'view testimonials' =>  array(
      'title' => t('View testimonials'),
    ),
    'delete testimonials' =>  array(
      'title' => t('Delete testimonials'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function testimonials_menu() {
	$testimonials_path = 'admin/structure/testimonials';
	
  $items[$testimonials_path] = array(
    'title' => 'Testimonials',
    'description' => 'Manage testimonials.',
    'page callback' => 'drupal_get_form',
		'page arguments' => array('testimonials_admin_overview_form'),
    'access arguments' => array('administer testimonials'),
    'file' => 'admin/testimonials.admin.inc',
  );
  $items[$testimonials_path . '/manage/%testimonials_type'] = array(
    'title' => 'View testimonials type',
    'title callback' => 'testimonials_type_page_title',
    'title arguments' => array(4),
    'page callback' => 'testimonials_information',
    'page arguments' => array(4),
    'access arguments' => array('administer testimonials'),
    'file' => 'admin/testimonials.admin.inc',
  );
  $items[$testimonials_path . '/manage/%testimonials_type/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items[$testimonials_path . '/add'] = array(
    'title' => 'Add new testimonial',
    'page callback' => 'testimonials_add_page',
    'access arguments' => array('create testimonials'),
    'weight' => -10,
    'menu_name' => 'management',
    'file' => 'admin/testimonials.admin.inc',
  );
  foreach (testimonials_types() as $type => $type_info) {
    $items[$testimonials_path . '/add/' . $type] = array(
      'title' => $type_info->label,
      'title callback' => 'check_plain',
      'page callback' => 'testimonials_add',
      'page arguments' => array(2),
      'access arguments' => array('create testimonials'),
      'description' => $type_info->description,
      'file' => 'admin/testimonials.admin.inc',
    );
  }
	
	$testimonials_uri = 'testimonials/%testimonials';
	
  $items[$testimonials_uri] = array(
    'title callback' => 'testimonials_page_title',
    'title arguments' => array(1),
    'page callback' => 'testimonials_page_view',
    'page arguments' => array(1),
    'access arguments' => array('view testimonials'),
    'type' => MENU_CALLBACK,
    'file' => 'admin/testimonials.pages.inc',
  );
  $items[$testimonials_uri . '/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items[$testimonials_uri . '/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'testimonials_page_edit',
    'page arguments' => array(1),
    'access arguments' => array('update testimonials'),
    'weight' => 0,
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    'file' => 'admin/testimonials.admin.inc',
  );
  $items[$testimonials_uri . '/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('testimonials_delete_confirm', 1),
    'access arguments' => array('delete testimonials'),
    'weight' => 1,
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'file' => 'admin/testimonials.admin.inc',
  );
	
  $items['admin/structure/testimonials/testimonials-types/%testimonials_type/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('testimonials_type_form_delete_confirm', 5),
    'access arguments' => array('administer testimonials types'),
    'weight' => 1,
    'type' => MENU_NORMAL_ITEM,
    'file' => 'admin/testimonials.admin.inc',
  );
	

  // Administration pages.
  $items['admin/structure/testimonials/settings'] = array(
    'title' => 'Testimonials settings',
    'description' => 'Manage testimonials display and other settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('testimonials_admin_settings'),
    'access arguments' => array('administer testimonials'),
    'file' => 'admin/testimonials.admin.inc',
		'weight' => 10,
  );

  return $items;
}

/**
 * Sets the page title based on the specified testimonial.
 *
 * @param $testimonial
 *   The testimonial object.
 */
function testimonials_page_title($testimonial) {
  return $testimonial->title;
}

/**
 * Implements hook_forms().
 *
 * All testimonials forms share the same form handler.
 */
function testimonials_forms() {
  $forms = array();
  if ($types = testimonials_types()) {
    foreach (array_keys($types) as $type) {
      $forms[$type . '_testimonials_form']['callback'] = 'testimonials_form';
    }
  }
  return $forms;
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function testimonials_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  // Add action link to 'node/add' on 'admin/content' page.
  if ($root_path == 'admin/structure/testimonials') {
    $item = menu_get_item('admin/structure/testimonials/add');
    if ($item['access']) {
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
      );
    }
  }
}

/**
 * Access callback for testimonials Type.
 */
function testimonials_access($op, $entity = NULL) {
  return user_access('administer testimonials');
}

/**
 * Access callback for testimonials Type.
 */
function testimonials_type_access($op, $entity = NULL) {
  return user_access('administer testimonials types');
}

/**
 * Load testimonials Type.
 */
function testimonials_type_load($testimonials_type) {
  return testimonials_types($testimonials_type);
}

/**
 * List of testimonials Types.
 */
function testimonials_types($type_name = NULL) {
  $types = entity_load_multiple_by_name('testimonials_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Save testimonials type entity.
 */
function testimonials_type_save($testimonials_type) {
  entity_save('testimonials_type', $testimonials_type);
}

/**
 * Delete single testimonials type.
 */
function testimonials_type_delete($testimonials_type) {
  entity_delete('testimonials_type', entity_id('testimonials_type' ,$testimonials_type));
}

/**
 * Delete multiple testimonials types.
 */
function testimonials_type_delete_multiple($testimonials_type_ids) {
  entity_delete_multiple('testimonials_type', $testimonials_type_ids);
}

/**
 * Menu title callback.
 *
 * @param $type
 *   The testimonial type.
 * @return string
 *   The human-friendly name of the testimonial type.
 */
function testimonials_type_page_title($type) {
  return t('Manage @type', array('@type' => $type->label));
}


/**
 * Load testimonials entities from the database.
 *
 * This function should be used whenever you need to load more than one testimonials
 * from the database. Testimonials are loaded into memory and will not require
 * database access if loaded again during the same page request.
 *
 * @see entity_load()
 *
 * @param $tids
 *   An array of testimonials IDs.
 * @param $conditions
 *   An array of conditions on the {testimonials} table in the form 'field' => $value.
 * @param $reset
 *   Whether to reset the internal entity_load cache.
 *
 * @return
 *   An array of node objects indexed by nid.
 */
function testimonials_load_multiple($tids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('testimonials', $tids, $conditions, $reset);
}

/**
 * Load an testimonials object from the database.
 *
 * @param $tid
 *   The testimonials ID.
 * @param $reset
 *   Whether to reset the testimonials_load_multiple cache.
 *
 * @return
 *   A fully-populated node object.
 */
function testimonials_load($tid = NULL, $conditions = array(), $reset = FALSE) {
  $tids = (isset($tid) ? array($tid) : array());
  $testimonials = testimonials_load_multiple($tids, $conditions, $reset);
  return $testimonials ? reset($testimonials) : FALSE;
}

/**
 * Save an testimonial.
 *
 * @param $testimonial
 *   The testimonial to be saved.
 * @return
 *   The saved testimonial, now with an tid if necessary.
 */
function testimonials_save($testimonial) {
  return entity_get_controller('testimonials')->save($testimonial);
}

/**
 * Returns an initialized testimonial object.
 *
 * @param $type
 *   The machine-readable type of the testimonial.
 *
 * @return
 *   An testimonial object with all default fields initialized.
 */
function testimonials_new($type = '') {
  return entity_get_controller('testimonials')->create($type);
}


/**
 * Deletes an testimonial by ID.
 *
 * @param $tid
 *   The ID of the product to delete.
 *
 * @return
 *   TRUE on success, FALSE otherwise.
 */
function testimonials_delete($tid) {
  return testimonials_delete_multiple(array($tid));
}

/**
 * Deletes multiple testimonials by ID.
 *
 * @param $tids
 *   An array of testimonial IDs to delete.
 *
 * @return
 *   TRUE on success, FALSE otherwise.
 */
function testimonials_delete_multiple($tids) {
  return entity_get_controller('testimonials')->delete($tids);
}

/**
 * Implements hook_image_style_delete().
 */
function testimonials_image_style_delete($style) {
  // If a style is deleted, update the variables.
  // Administrators choose a replacement style when deleting.
  testimonials_image_style_save($style);
}

/**
 * Implements hook_image_style_save().
 */
function testimonials_image_style_save($style) {
  // If a style is renamed, update the variables that use it.
  if (isset($style['old_name']) && $style['old_name'] == variable_get('testimonials_picture_style', '')) {
    variable_set('testimonials_picture_style', $style['name']);
  }
}

/**
 * Implements hook_file_download().
 *
 * Ensure that testimonials pictures (avatars) are always downloadable.
 */
function testimonials_file_download($uri) {
  if (strpos(file_uri_target($uri), variable_get('testimonials_picture_path', 'testimonials') . '/testimonials-') === 0) {
    $info = image_get_info($uri);
    return array('Content-Type' => $info['mime_type']);
  }
}

/**
 * Implements hook_file_move().
 */
function testimonials_file_move($file, $source) {
  // If a testimonial's picture is replaced with a new one, update the record in
  // the testimonials table.
  if (isset($file->fid) && isset($source->fid) && $file->fid != $source->fid) {
    db_update('testimonials')
      ->fields(array(
        'author_picture' => $file->fid,
      ))
      ->condition('author_picture', $source->fid)
      ->execute();
  }
}

/**
 * Implements hook_file_delete().
 */
function testimonials_file_delete($file) {
  // Remove any references to the file.
  db_update('testimonials')
    ->fields(array('author_picture' => 0))
    ->condition('author_picture', $file->fid)
    ->execute();
}

/**
 * Implements hook_block_info().
 *
 * @return array
 */
function testimonials_block_info() {
  $blocks = array();
	$blocks_count = variable_get('testimonials_blocks_count', 1);
	
	for ($i = 1; $i <= $blocks_count; $i++){
		$blocks['testimonials_block_' . $i] = array(
			'info' => t('Testimonials block !block_number', array('!block_number' => $i)),
		);
	}
	
  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * @param $delta
 * @return mixed
 */
function testimonials_block_view($delta) {
	$settings = variable_get('testimonials_block_settings_' . $delta, testimonials_options('default_settings'));
	$settings['id'] = 'testimonials-block-' . $delta;
	$query = db_select('testimonials', 't');
	$query = $query
		->fields('t')
		->orderBy('weight', 'ASC')
		->condition('status', 0, '>');
		
	if(!empty($settings['types']) && count($settings['types']) > 0){
		$query = $query->condition('type', $settings['types'], 'IN');
	}	
	
	if(isset($settings['count']) && (int)$settings['count'] > 0){
		$query = $query->range(0, $settings['count']);
	}	
	
	$testimonials = $query
		->execute();
		
	$block['subject'] = t('Our Testimonials');
	
	// enable owlcarousel
	if(module_exists('owlcarousel') && isset($settings['owlcarousel_settings']['enable_carousel']) && $settings['owlcarousel_settings']['enable_carousel']){
		$block['content'] = theme('testimonials_carousel', array('testimonials' => $testimonials, 'options' => $settings));
	}
	else {
		$block['content'] = theme('testimonials_list', array('testimonials' => $testimonials, 'options' => $settings));
	}

  return $block;
}

/**
 * Implements hook_block_configure().
 */
function testimonials_block_configure($delta) {
  if (strpos($delta, 'testimonials') !== FALSE) {
		$settings = variable_get('testimonials_block_settings_' . $delta, testimonials_options('default_settings'));
		$form = array();
		$form_state = array();
		$form = testimonials_settings_form($form, $form_state, $settings);
    return $form;
  }
}

/**
 * Implements hook_block_save().
 */
function testimonials_block_save($delta, $edit) {
	if(isset($edit['testimonials_settings'])){
		variable_set('testimonials_block_settings_' . $delta, $edit['testimonials_settings']);
	}
}


/**
 * Process variables for testimonials-list.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $testimonials: An array of testiomonils objects
 * - $options: An array of testiomonils settings
 *   fields.
 *
 * @see testimonials-list.tpl
 */
function template_preprocess_testimonials_list(&$variables) {
	$options = $variables['options'];
	$testimonials = $variables['testimonials'];
	$attributes = array(
		'class' => _testimonials_css_classes($options),
		'id' => drupal_html_id($options['id'] . '-wrapper'),
	);
	
	$items = array();
	if (!empty($testimonials)) {
		foreach($testimonials as $testimonial){
			$items[] = theme('testimonials_list_item', array('testimonial' => $testimonial, 'options' => $options));
		}
	}
	$variables['wrapper_attributes'] = drupal_attributes($attributes);
	$variables['items'] = $items;
}


/**
 * Process variables for testimonials-carousel.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $testimonials: An array of testiomonils objects
 * - $options: An array of testiomonils settings
 *   fields.
 *
 * @see testimonials-carousel.tpl
 */
function template_preprocess_testimonials_carousel(&$variables) {
	$options = $variables['options'];
	$testimonials = $variables['testimonials'];

	$attributes = array(
		'class' => _testimonials_css_classes($options),
		'id' => drupal_html_id($options['id'] . '-wrapper'),
	);
	
	//owlcarousel options
	$owlcarousel_options = array();
	$owlcarousel_options['attributes']['id']['default'] = drupal_html_id($options['id']);
	$owlcarousel_options['instance'] = $options['owlcarousel_settings']['instance'] ? $options['owlcarousel_settings']['instance'] : 'owlcarousel_settings_default';
	
	$items = array();
	if (!empty($testimonials)) {
		foreach($testimonials as $testimonial){
			$item['row'] = theme('testimonials_list_item', array('testimonial' => $testimonial, 'options' => $options));
			$items[] = $item;
		}
	}
	
	$variables['wrapper_attributes'] = drupal_attributes($attributes);
	$variables['carousel'] = theme('owlcarousel', array('items' => $items, 'settings' => $owlcarousel_options));
}

/**
 * Process variables for testimonials-list-item.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $testimonial: A testiomonils object
 * - $options: An array of testiomonils settings
 *   fields.
 *
 * @see testimonials-list-item.tpl
 */
function template_preprocess_testimonials_list_item(&$variables) {
	$testimonial = $variables['testimonial'];
	$options = $variables['options'];
	
	$show_title = _testimonials_get_bool_option('show_title', $options);
	$show_picture = _testimonials_get_bool_option('show_picture', $options);
	$show_rating = _testimonials_get_bool_option('show_rating', $options);
	$composition = _testimonials_get_option('composition', $options);

	$message = '';
	$author_info = '';
	
	if($show_picture){
		$author_info .= theme('testimonials_item', array('element' => array(
			'#title' => t('Autohr picture'),
			'#attributes' => array('class' => array('testimonial-author-picture')),
			'#markup' => theme('testimonials_picture', array('testimonial' => $testimonial)),
		)));
	}
	// Show title
	if($show_title){
		$author_info .= theme('testimonials_item', array('element' => array(
			'#title' => t('Title'),
			'#attributes' => array('class' => array('testimonial-title')),
			'#markup' => check_plain($testimonial->title),
		)));
	}
	$author_info .= theme('testimonials_item', array('element' => array(
		'#title' => t('Autohr name'),
		'#attributes' => array('class' => array('testimonial-author-name')),
		'#markup' => theme('testimonials_author_name', array('testimonial' => $testimonial)),
	)));
	if($show_rating){
		$author_info .= theme('testimonials_item', array('element' => array(
			'#title' => t('Rating'),
			'#attributes' => array('class' => array('testimonial-rating')),
			'#markup' => theme('testimonials_rating', array('testimonial' => $testimonial)),
		)));
	}
	
	$message = theme('testimonials_item', array('element' => array(
		'#title' => t('Autohr picture'),
		'#attributes' => array('class' => array('testimonial-message')),
		'#markup' => check_plain($testimonial->message),
	)));
	
	$variables['message'] = $message;
	$variables['author_info'] = $author_info;
	$variables['composition'] = $composition;
}

/**
 * Process variables for theme rating
 *
 * The $variables array contains the following arguments:
 * - $testimonial: A testimonial object
 *   fields.
 *
 */
function theme_testimonials_rating(&$variables) {
	$testimonial = $variables['testimonial'];
	if(empty($testimonial->rating) || !$testimonial->rating){
		return;
	}
	$rating_count = $variables['rating_count'];
	
	$output = '<ul class="testimonials-rating testimonials-item clearfix">';
	for($i = 1; $i <= $rating_count; $i++){
		if($i <= $testimonial->rating)
			$output .= '<li class="active"></li>';
			
		else
			$output .= '<li></li>';
	}
	$output .= '</ul>';
	return $output;
}

/**
 * Process variables for theme author name
 *
 * The $variables array contains the following arguments:
 * - $testimonial: A testimonial object
 *   fields.
 *
 */
function theme_testimonials_author_name(&$variables) {
	$testimonial = $variables['testimonial'];
	if(empty($testimonial->author_name)){
		return;
	}
	$company = empty($testimonial->company) ? '' : "-" . check_plain($testimonial->company);
	if(empty($testimonial->author_link)){
		return check_plain($testimonial->author_name) . "$company";	
	}
	else {
		return l(check_plain($testimonial->author_name), $testimonial->author_link) . "$company";	
	}
}

/**
 * Process variables for testimonials-picture.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $testimonial: A testimonial object with 'name', 'uid' and 'picture'
 *   fields.
 *
 * @see testimonials-picture.tpl.php
 */
function template_preprocess_testimonials_picture(&$variables) {
		$variables['testimonials_picture'] = '';
    $testimonial = $variables['testimonial'];
    if (!empty($testimonial->author_picture)) {
      // @TODO: Ideally this function would only be passed file objects, but
      // since there's a lot of legacy code that JOINs the {testimonials} table to
      // {node} or {comments} and passes the results into this function if we
      // a numeric value in the picture field we'll assume it's a file id
      // and load it for them. Once we've got testimonials_load_multiple()
      // functions the testimonials module will be able to load
      // the picture files in mass during the object's load process.
      if (is_numeric($testimonial->author_picture)) {
        $testimonial->author_picture = file_load($testimonial->author_picture);
      }
      if (!empty($testimonial->author_picture->uri)) {
        $filepath = $testimonial->author_picture->uri;
      }
    }
    elseif (variable_get('testimonials_picture_default', '')) {
      $filepath = variable_get('testimonials_picture_default', '');
    }
    if (isset($filepath)) {
      $alt = t("@testimonial's picture", array('@testimonial' => $testimonial->title));
      // If the image does not have a valid Drupal scheme (for eg. HTTP),
      // don't load image styles.
      if (module_exists('image') && file_valid_uri($filepath) && $style = variable_get('testimonials_picture_style', '')) {
        $variables['testimonials_picture'] = theme('image_style', array('style_name' => $style, 'path' => $filepath, 'alt' => $alt, 'title' => $alt));
      }
      else {
        $variables['testimonials_picture'] = theme('image', array('path' => $filepath, 'alt' => $alt, 'title' => $alt));
      }
      if (!empty($testimonial->author_link)) {
        $attributes = array('attributes' => array('title' => $testimonial->author_name), 'html' => TRUE);
        $variables['testimonials_picture'] = l($variables['testimonials_picture'], $testimonial->author_link, $attributes);
      }
    }
}

/**
 * Process variables for testimonials-item.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $element
 *
 * @see testimonials-item.tpl.php
 */
function template_preprocess_testimonials_item(&$variables) {
  $variables['title'] = $variables['element']['#title'];
  $variables['value'] = $variables['element']['#markup'];
  $variables['item_attributes'] = '';
  if (isset($variables['element']['#attributes'])) {
    $variables['item_attributes'] = drupal_attributes($variables['element']['#attributes']);
  }
}

/**
 * Testimonials settings form
 */
function testimonials_settings_form(&$form, &$form_state, $settings) {
	$form['testimonials_settings'] = array(
		'#type' => 'fieldset',
		'#title' => t('Testimonials settings'),
		'#tree' => TRUE
	);
	foreach (testimonials_types() as $key => $type) {
		$types[$key] = $type->label;
	}
	$disabled = count($types) == 1 ? TRUE : FALSE;
	$form['testimonials_settings']['types'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Types'),
		'#options' => $types,
		'#disabled' => $disabled,
		'#description' => t('Select testimonials types to show in this block.'),
	);
	if(isset($settings['types']) && !empty($settings['types'])){
		$form['testimonials_settings']['types']['#default_value'] = $settings['types'];
	}
	$form['testimonials_settings']['count'] = array(
		'#type' => 'textfield',
		'#title' => t('Limit'),
		'#default_value' => $settings['count'],
		'#description' => t('Limit testimonials to the selected value. 0 for unlimited'),
	);
	$form['testimonials_settings']['show_title'] = array(
		'#type' => 'checkbox',
		'#title' => t('Show title'),
		'#default_value' => $settings['show_title'],
		'#description' => t('Hide/show testimonials title.'),
	);
	$form['testimonials_settings']['show_picture'] = array(
		'#type' => 'checkbox',
		'#title' => t('Show picture'),
		'#default_value' => $settings['show_picture'],
		'#description' => t('Hide/show testimonials picture.'),
	);
	$form['testimonials_settings']['show_rating'] = array(
		'#type' => 'checkbox',
		'#title' => t('Show rating'),
		'#default_value' => $settings['show_rating'],
		'#description' => t('Hide/show testimonials rating.'),
	);
	$form['testimonials_settings']['picture_shape'] = array(
		'#type' => 'select',
		'#title' => t('Picture shape'),
		'#default_value' => $settings['picture_shape'],
		'#options' => array('default' => 'Default') + testimonials_options('picture_shape'),
		'#description' => t('Testimonials picture shape.'),
	);
	$form['testimonials_settings']['theme'] = array(
		'#type' => 'select',
		'#title' => t('Theme'),
		'#default_value' => $settings['theme'],
		'#options' => array('default' => 'Default') + testimonials_options('themes'),
		'#description' => t('Choose testimonials theme apply to this block.'),
	);
	$form['testimonials_settings']['composition'] = array(
		'#type' => 'select',
		'#title' => t('Composition'),
		'#default_value' => $settings['composition'],
		'#options' => array('default' => 'Default') + testimonials_options('composition'),
		'#description' => t('Choose testimonials composition apply to this block.'),
	);
	$form['testimonials_settings']['alignment'] = array(
		'#type' => 'select',
		'#title' => t('Alignment'),
		'#default_value' => $settings['alignment'],
		'#options' => array('default' => 'Default') + testimonials_options('alignment'),
		'#description' => t('Choose testimonials alignment apply to this block.'),
	);
	
	// owlcarousel options
	if(module_exists('owlcarousel')){
		$keys = owlcarousel_instance_callback_list();
		$form['testimonials_settings']['owlcarousel_settings'] = array(
			'#type' => 'fieldset',
			'#title' => t('Owlcarousel settings'),
		);
		$form['testimonials_settings']['owlcarousel_settings']['enable_carousel'] = array(
			'#type' => 'checkbox',
			'#title' => t('Enable carousel'),
			'#default_value' => $settings['owlcarousel_settings']['enable_carousel'],
			'#description' => t('Enable or disable testimonials carousel.'),
		);
		$form['testimonials_settings']['owlcarousel_settings']['instance'] = array(
			'#type' => 'select',
			'#title' => t('Options set'),
			'#default_value' => $settings['owlcarousel_settings']['instance'],
			'#options' => $keys,
			'#description' => t('This options set will be apply on testimonials carousel.!url to add new owlcarousel options set.', array('!url' => l('Click here', 'admin/config/user-interface/owlcarousel'))),
		);
	}
	
	// allow other modules to alter tsetimonials settings form
	drupal_alter('testimonials_settings_form', $form, $form_state, $settings);
	
	return $form;
}

/**
 * helper function return options list
 */
function testimonials_options( $option_group = false ) {
	$options = array(
		'themes' => array(
			'speech' => t('Rounded Speech Bubbles'),
			'flat' => t('Flat Speech Bubbles'),
			'card' => t('Flat Card Box'),
			'quotes' => t('Quote Signs'),
			'none' => t('None'),
		),
		'composition' => array(
			'info_below' => t('Author Below, Text Above'),
			'info_above' => t('Author Above, Text Below'),
			'info_left' => t('Author Left, Text Right'),
			'info_right' => t('Author Right, Text Left'),
		),
		'alignment' => array(
			'left' => t('Left'),
			'right' => t('Right'),
			'center' => t('Center'),
		),
		'picture_shape' => array(
			'rounded' => t('Rounded'),
			'falt' => t('Flat'),
		),
		'default_settings' => array(
			'count' => 0,
			'theme' => variable_get('testimonials_default_theme', 'speech'),
			'composition' => variable_get('testimonials_default_composition', 'info_above'),
			'alignment' => variable_get('testimonials_default_alignment', 'center'),
			'picture_shape' => variable_get('testimonials_picture_shape', 'rounded'),
			'owlcarousel_settings' => array(
				'instance' => 'owlcarousel_settings_default',
				'enable_carousel' => TRUE,
			),
			'types' => array(
				'testimonials' => 'testimonials',
			),
			'show_title' => TRUE,
			'show_picture' => TRUE,
			'show_rating' => TRUE,
		),
	);
	
	// allow other modules to alter default options
	drupal_alter('testimonials_options', $options);
	
	return $option_group && isset($options[$option_group]) ? $options[$option_group] : $options;
}

/**
 * helper function return required option
 */
function _testimonials_get_option($option_name, $settings = array()) {
	$default = testimonials_options('default_settings');
	return ( isset($settings[$option_name]) 
		&& $settings[$option_name] != 'default' ) 
		? $settings[$option_name] : $default[$option_name];
}

/**
 * helper function return required boolen option
 */
function _testimonials_get_bool_option($option_name, $settings = array()) {
	$default = testimonials_options('default_settings');
	return ( isset($settings[$option_name]) ) 
		? $settings[$option_name] : $default[$option_name];
}

/**
 * helper function return css classes to define testimonials theme
 */
function _testimonials_css_classes($settings = array()) {
	return array(
		'testimonials-template',
		'testimonials_theme_' . _testimonials_get_option('theme', $settings),
		'testimonials_composition_' . _testimonials_get_option('composition', $settings),
		'testimonials_alignment_' . _testimonials_get_option('alignment', $settings),
		'testimonials_picture_' . _testimonials_get_option('picture_shape', $settings),
	);
}