<?php
/**
 * @file
 * Hooks provided by this module.
 */

/**
 * @addtogroup hooks
 * @{
 */
/**
 * Alter default testimonials options.
 *
 * @param array $options
 *   An array of default options, keyed by options group.
 *
 * @see testimonials_options()
 */
function hook_testimonials_options_alter(array &$options) {
	if(isset($options['themes'])){
		$options['themes']['new_theme'] = 'New theme';
	}
}

/**
 * Alter default testimonials options.
 *
 * @param array $form
 *   An array of form elements
 *
 * @param array $form_state
 *   An array of form state elements
 *
 * @param array $settings
 *   An array of testimonials settings
 *
 * @see testimonials_settings_form()
 */
function hook_testimonials_settings_form_alter(&$form, &$form_state, $settings) {
	
}
