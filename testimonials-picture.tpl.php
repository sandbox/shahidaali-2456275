<?php

/**
 * @file
 * Default theme implementation to present a picture configured for the
 * testimonial.
 *
 * Available variables:
 * - $testimonials_picture: Image set by the testimonials or the site's default.
 * - $testimonial: Array of testimonial information. Potentially unsafe. Be sure to
 *   check_plain() before use.
 *
 * @see template_preprocess_testimonials_picture()
 *
 * @ingroup themeable
 */
?>
<?php if ($testimonials_picture): ?>
    <?php print $testimonials_picture; ?>
<?php endif; ?>
